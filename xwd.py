#!/usr/bin/env python

import fileinput
import codecs
import latex
import re
import sys

import argparse

parser = argparse.ArgumentParser(
	description = "Render crosswords using LaTeX.")
parser.add_argument("infile", metavar = "FILE",
	help = "a file containing a crossword definition")

group = parser.add_mutually_exclusive_group()
group.add_argument("-n", "--force-new", default = 0,
	action = "store_const", dest = "style", const = 1,
	help = "assume new-style solution definitions")
group.add_argument("-o", "--force-old",
	action = "store_const", dest = "style", const = 2,
	help = "assume old-style solution definitions")
parser.add_argument("-b", "--british",
	action = "store_true",
	help = "fill in empty grid positions with black boxes")
parser.add_argument("-s", "--scale",
	action = "store", type = float, default = 1.0,
	help = "scale the crossword by the given factor")

args = parser.parse_args()

latex.register()

# print preamble
print """
\\documentclass{article}
\\usepackage{times}
\\usepackage[T1]{fontenc}
\\usepackage{tikz}
\\pagestyle{empty}
\\usepackage{geometry}

\\usepackage{layout}

\\geometry{
  includeheadfoot,
  margin=2cm
}
\\begin{document}
"""


# Parse the input into three parts.

(grid, across, down) = ([],[],[])

state = -1 # become 0, 1, 2 during parsing
for line in codecs.open(args.infile,encoding='utf-8'):
	line = line.rstrip()
	if not line: 
			state += 1
			continue

	# if the source includes a line like this:
	# % no. <digits>
	# then print the <digits> as the title of the crossword
	match = re.findall(r'\% no. (\d+)',line)
	if match: 
		print '\\noindent{\\huge ',match[0],'}'
		print 
		print '\\bigskip'
	# skip anything else
	if line[0] == '%': continue

	# Put current line into right part, depending on state
	(grid, across, down)[state].append(line)

if args.style == 0:
	# Autodetect which definition style we should use: if all solution
	# lines contain a colon, use new-style solutions
	for i in across + down:
		if not ':' in i:
			args.style = 2
			break
	else:
		args.style = 1

# Build the crossword

# Compute bounding box dimensions (rows,cols) 

rows = len(grid)
cols = len(max(grid, key = len)) # clever python builtin max function

# Fill 2D dictionary such that
# C[r][c]['letter']: the letter at (r,c), or ''
# C[r][c]['startsacross']: which across-word starts here, if any (initially, none)
# C[r][c]['startsdown']: which down-word starts here, if any (initially, none)
# C[r][c]['endsacross']: does an across-word end here? (initally, False)
# C[r][c]['endsacross']: does an across-word end here? (initally, False)

C = [[{'letter':''} for c in range(cols)] for r in range(rows)]
for r in range(rows):
	for c in range(cols):
		if c < len(grid[r]):
			C[r][c]['letter'] = grid[r][c].strip()
		C[r][c]['startsacross'] = None
		C[r][c]['startsdown'] = None	
		C[r][c]['endsacross'] = False
		C[r][c]['endsdown'] = False	

# Place the across and down clues

clues = {} # want clues['OAK'] = 'All right around the tree.'

non_character = re.compile(r"([^\w]+)", re.UNICODE)
def to_grid_representation(word):
	return non_character.sub("", word)

# Build a list acrosswords including all across-words in top-bottom, left-
# right order (i.e., as they appear in the source)

acrosswords = []

for line in across:
	words = line.split(":" if args.style == 1 else " ", 1)
	word, clues[word] = words[0].strip(), words[1].strip()
	acrosswords.append(word)

# Now the work starts
# Iterate through acrosswords, trying to place them in the grid one by one

(r,c) = (0,0) # top left corner
i = 0         # currently placing the i'th across-word 

while i < len(acrosswords):
	# make (r,c) next nonempty cell
	while r < rows and c < cols and C[r][c] == '':
		c += 1
		if c == cols: (r,c) = (r+1, 0)

	word = acrosswords[i]
	gword = to_grid_representation(word)

	cc = c
	matched = True
	# try to put word at (r,c)
	for letter in gword:
		if cc >= cols or letter != C[r][cc]['letter']: matched = False
		cc += 1
	if matched:
		# Note this at first and last cell for this word in grid 
		# (so that we can later put fences and clue numbers there)
		C[r][c]['startsacross'] = word
		C[r][cc-1]['endsacross'] = True
		i += 1
		c = cc
	else: 
		# couldn't match word at (r,c): ignore this cell, proceed to next
		# TODO: maybe the system should complain when this happens too often
		c += 1 
	if c == cols: (r, c) = (r+1,0)
	if r == rows and not matched:
		print("exhausted grid without placing {0}".format(
			word.encode('latex')))

# Same thing for the down-words, exchanging rows for columns etc.
# TODO: Would be cleaner to do it only once, a direction-agnostic fashion,
#       but that requires hairy index fiddling

downwords = []

for line in down:
	words = line.split(":" if args.style == 1 else " ", 1)
	word, clues[word] = words[0].strip(), words[1].strip()
	downwords.append(word)

(r,c) = (0,0)
i = 0

while i < len(downwords):
	# make (r,c) next nonempty cell
	while c < cols and r < rows and C[r][c] == '':
		r += 1
		if r == rows: (r,c) = (0, c+1)

	word = downwords[i]
	gword = to_grid_representation(word)

	rr = r # lookahead row pointer
	matched = True
	# try to put word at (r,c)
	for letter in gword:
	#	if rr < rows: print letter, C[rr][c]['letter']
		if rr >= rows or letter != C[rr][c]['letter']: matched = False
		rr += 1
	if matched:
		# Note this at first and last cell for this word in grid 
		# (so that we can later put fences and clue numbers there)
	#	print word,r,c
		C[r][c]['startsdown'] = word
		C[rr-1][c]['endsdown'] = True
		i += 1
		r = rr
	else:
		r += 1
	if r == rows: (r, c) = (0,c+1)
	if c == cols and not matched:
		print("exhausted grid without placing {0}".format(
			word.encode('latex')))

(acrosscounter, downcounter) = ({},{})

# Print the puzzle
# Unremarkable TikZ 

print "\\centerline{{\\begin{{tikzpicture}}[scale = {0}, every node/.style = {{scale = {0}}}]".format(args.scale)
cluecounter = 1
for r in range(rows):
	for c in range(cols):
		if C[r][c]['letter'] != '':
			print '\\node[minimum size = 1cm, draw] at ({0},{1}) {{}};'.format(c, -r)
		elif args.british:
			print '\\node[minimum size = 1cm, draw, fill = black] at ({0},{1}) {{}};'.format(c, -r)
		clue_starts_here = False
		word = C[r][c]['startsacross']
		if word:
			acrosscounter[word] = cluecounter
			clue_starts_here = True
		word = C[r][c]['startsdown'] 
		if word:
			downcounter[word] = cluecounter
			clue_starts_here = True
		if clue_starts_here: 
			print '\\node[anchor = north west, font = \\sf\\footnotesize] at ({0},{1}) {{{2}}};'.format(c-.5,-r+.5,cluecounter)
			cluecounter += 1

		fences = ''
		if C[r][c]['startsacross'] and c > 0 and C[r][c-1]['letter'] != '':
			print '\\draw [ultra thick] ({0},{1}) -- ({0},{2});'.format(c-.5,-r+.5,-r-.5)
		if C[r][c]['startsdown'] and r > 0 and C[r-1][c]['letter'] != '': 
			print '\\draw [ultra thick] ({0},{1}) -- ({2},{1});'.format(c-.5,-r+.5,c+.5)
		if C[r][c]['endsacross'] and c < cols-1 and C[r][c+1]['letter'] != '':
			print '\\draw [ultra thick] ({0},{1}) -- ({0},{2});'.format(c+.5,-r+.5,-r-.5)
		if C[r][c]['endsdown'] and r < rows-1 and C[r+1][c]['letter'] != '': 
			print '\\draw [ultra thick] ({0},{1}) -- ({2},{1});'.format(c-.5,-r-.5,c+.5)

print "\\end{tikzpicture}}"
print "\\bigskip"

# We turn to the clues.
# Replace placeholders like '{{OAK}}' with their coordinate like '20 vandret'

def to_position(word):
	""" Returns something like '20 vandret' for a given word"""
	if word in acrosscounter:
		return "{0} vandret".format(acrosscounter[word])
	if word in downcounter:
		return "{0} lodret".format(downcounter[word])
	print "* Fatal: could not locate position of ", word

for word, clue in clues.iteritems():
	for bracket_word in re.findall(r'\{\{(.*?)\}\}',clue):  # look for '{{OAK}}'
		coord = to_position(bracket_word) 					# coord = '20 vandret'
		bracket_word_in_braces = '{{{{{0}}}}}'.format(bracket_word.encode('latex'))
		clue = clue.encode('latex').replace(bracket_word_in_braces, coord)
	clues[word] = clue # I burn in hell: modify a dictionary while iterating over it

# Print the clues

def length_indicator(word):
	result = []
	i = False
	for seg in filter(lambda x: len(x) != 0, non_character.split(word)):
		result.append(
			str(len(seg)) if not i
			else ("," if seg == " " else seg.strip()))
		i = not i
	return "({0})".format("".join(result))

print "\\noindent\\textbf{Vandret.}"
for word in acrosswords:
	print "{{\\bf {0}.}}~{1}~\mbox{{{2}}}".format(acrosscounter[word],
		clues[word].encode('latex'), length_indicator(word))

print
print "\\noindent\\textbf{Lodret.}"
for word in sorted(downwords, key = lambda k: downcounter[k]):
	print "{{\\bf {0}.}}~{1}~\mbox{{{2}}}".format(downcounter[word],
		clues[word].encode('latex'), length_indicator(word))

# Postamble

print "\\end{document}"
